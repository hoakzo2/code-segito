var database = [];

document.addEventListener("DOMContentLoaded", function() {
    const search_bar = document.getElementById("search_bar");
    if (search_bar) {
        search_bar.addEventListener("input", Find);
    } else {
        console.error("Search bar element not found.");
    }

    LateLoad();
});

function LateLoad() {
    MakeDatas();
    Update(); // Ensure this starts the loop
}

function Update() {
    requestAnimationFrame(Update);
}

function Find() {
    const search_bar = document.getElementById("search_bar");
    if (!search_bar) {
        console.error("Search bar element not found.");
        return;
    }

    const text = search_bar.value.toLowerCase();
    const talalatok = document.querySelector("[talalatok-hely]");
    if (!talalatok) {
        console.error("Element with [talalatok-hely] not found.");
        return;
    }
    
    talalatok.innerHTML = ""; // Clear previous results

    const filteredResults = database.filter(e => 
        e.name.toLowerCase().includes(text) || 
        e.desc.toLowerCase().includes(text) || 
        e.tags.some(tag => tag.toLowerCase().includes(text))
    );

    filteredResults.forEach(e => {
        const template = document.querySelector("[talalat-template]");
        if (template) {
            const resoult = template.content.cloneNode(true).children[0];
            const cim = resoult.querySelector("[cim-hely]");
            const desc = resoult.querySelector("[alairas-hely]");
            if (cim && desc) {
                cim.textContent = e.name;
                desc.textContent = e.desc;
                resoult.addEventListener("click", function() {
                    BigCode(e);
                });
                talalatok.append(resoult);
            } else {
                console.error("Element with [cim-hely] or [alairas-hely] not found in template.");
            }
        } else {
            console.error("Element with [talalat-template] not found.");
        }
    });
}

function BigCode(element) {
    const big_code = document.getElementById("big_code");
    if (!big_code) {
        console.error("Element with ID 'big_code' not found.");
        return;
    }

    if (element) {
        big_code.style.display = "flex";
        const cim = big_code.querySelector("[cim]");
        const tartalom = big_code.querySelector("[alairas-hely]");
        const code = big_code.querySelector("[code-hely]");
        const vonal = big_code.querySelector("[vonal]");
        const X = document.getElementById("X");
        const copy = document.getElementById("copy");
        if (cim && tartalom) {
            cim.textContent = element["name"];
            tartalom.innerHTML = element["tartalom"];
            if (element["code"] == "") {
                code.style.display = "none";
                vonal.style.display = "none";
                copy.style.display = "none";
                X.style.width = "100%";
            } else {
                X.style.width = "50%";
                copy.style.display = "block";
                code.style.display = "block";
                vonal.style.display = "block";
                code.innerHTML = element["code"];
            }
        } else {
            console.error("Element with [cim] or [alairas-hely] not found.");
        }
    } else {
        big_code.style.display = "none";
    }
}

function MakeResoults() {
    database.forEach(e => {
        const template = document.querySelector("[talalat-template]");
        if (template) {
            const resoult = template.content.cloneNode(true).children[0];
            const talalatok = document.querySelector("[talalatok-hely]");
            if (talalatok) {
                const cim = resoult.querySelector("[cim-hely]");
                const desc = resoult.querySelector("[alairas-hely]");

                if (cim && desc) {
                    cim.textContent = e["name"];
                    desc.textContent = e["desc"];
                    resoult.addEventListener("click", function() {
                        BigCode(e);
                    });
                    talalatok.append(resoult);
                } else {
                    console.error("Missing one of the required elements [cim-hely], [alairas-hely], or [code-hely].");
                }
            } else {
                console.error("Element with [talalatok-hely] not found.");
            }
        } else {
            console.error("Element with [talalat-template] not found.");
        }
    });
}

function AddListeners() {
    const X = document.getElementById("X");
    const copy = document.getElementById("copy");
    const sb_delete = document.querySelectorAll("[search-bar-delete]");
    X.addEventListener("click", function() {
        BigCode();
        copy.textContent = "Másolás";
    });
    copy.addEventListener("click", function() {
        const code = document.querySelector("[code-hely]");
        var copyable = code.innerHTML;
        while (copyable.includes("<br>")) {
            copyable = copyable.replace("<br>", "\n");
        }
        navigator.clipboard.writeText(copyable);
        copy.textContent = "Másolva!";
        setTimeout(function() {
            copy.textContent = "Másolás";
        }, 1000)
    });
    sb_delete.forEach(e => {
        e.addEventListener("click", function() {
            const search_bar = document.getElementById("search_bar");
            search_bar.value = "";
            Find();
        })
    });
}

function ClickedOn(element) {
    const big_code = document.getElementById("big_code");
    if (big_code.style.display == "flex") {
        if (element.querySelector("[talalat]")) {
            big_code.style.display = "none";
        }
    }
}

document.addEventListener("mousedown", function(event) {
    ClickedOn(event.target);
});
