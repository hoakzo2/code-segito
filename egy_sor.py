try:
    import pyperclip
except ImportError:
    print("No pyperclip")

_input = []

with open("py.json", "r", encoding="utf-8") as file:
    lines = file.readlines()
    for i in lines:
        i = i.replace("\t", "")
    _input = lines

_input = " ".join(_input)
_input = _input.replace("\n", " ").replace("\t", " ")

try:
    pyperclip.copy(_input)
except:
    pass
print(_input)